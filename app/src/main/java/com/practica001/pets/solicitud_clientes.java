package com.practica001.pets;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.practica001.pets.panel.panel_cliente;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class solicitud_clientes extends AppCompatActivity {

    private EditText etNombreM;
    private EditText etRaza;
    private EditText etPeso;
    private EditText etEdad;
    private EditText etPrecio;
    private RadioButton rdpaseo;
    private RadioButton rdbanar;
    private RadioButton rdalimentar;

    private Button btGPS;
    private Button btGuardar;
    private TextView Ubicacion;


    //mae la solicitud tiene que llevar un estado y ese estado puede ser pendiente, aceptado y finalizado.
//estas solicitudes se deben de guardar en cada cliente y meterlas en un tipo lista.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitud_clientes);

        etNombreM =  (EditText) findViewById(R.id.etNombreM);
        etRaza =  (EditText) findViewById(R.id.etRaza);
        etPeso =  (EditText) findViewById(R.id.etPeso);
        etEdad =  (EditText) findViewById(R.id.etEdad);
        etPrecio =  (EditText) findViewById(R.id.etPrecio);
        rdpaseo =   (RadioButton) findViewById(R.id.radioButton);
        rdbanar =     (RadioButton)findViewById(R.id.radioButton2);
        rdalimentar =  (RadioButton)  findViewById(R.id.radioButton3);



        Ubicacion = (TextView)findViewById(R.id.Ubicacio);
        btGPS = (Button)findViewById(R.id.btGPS);
        btGuardar = (Button)findViewById(R.id.btGuardar);

        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //registrocliente();
                SharedPreferences prefs = getSharedPreferences("shared_login_data",   Context.MODE_PRIVATE);
                String cliente = prefs.getString("email", "");

                String nombre =(etNombreM.getText().toString());
                String raza =(etRaza.getText().toString());
                String peso =(etPeso.getText().toString());
                String edad =(etEdad.getText().toString());
                String precio =(etPrecio.getText().toString());
                String paseo;
                String banar;
                String alimentar;
                if(rdbanar.isChecked()) {
                    banar = "1";
                }else{
                    banar = "0";
                }
                if(rdpaseo.isChecked()) {
                    paseo = "1";
                }else{
                    paseo = "0";
                }
                if(rdalimentar.isChecked()) {
                    alimentar = "1";
                }else{
                    alimentar = "0";
                }

                String ubicacion =(Ubicacion.getText().toString());

                new ConsumeWS().execute(nombre, raza, peso, edad,precio,paseo,banar,alimentar,cliente,ubicacion);//Llama al proceso del servicio web
            }
        });

        btGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager locationManager = (LocationManager) solicitud_clientes.this.getSystemService(Context.LOCATION_SERVICE);

                LocationListener locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {

                        //para guardar en la base de datos son esos parametros location.getLatitude() y location.getLongitude()
                        Ubicacion.setText(""+location.getLatitude()+" "+location.getLongitude());
                        Toast.makeText(getApplication(),"Ubicacion Actualizada",Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {


                    }

                    @Override
                    public void onProviderEnabled(String provider) {

                    }

                    @Override
                    public void onProviderDisabled(String provider) {

                    }
                };
                int permissionCheck = ContextCompat.checkSelfPermission(solicitud_clientes.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            }
        });
        int permissionCheck = ContextCompat.checkSelfPermission(solicitud_clientes.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck== PackageManager.PERMISSION_DENIED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)){

            }else{
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    private class ConsumeWS extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String nombre=params[0];
            String raza=params[1];
            String peso=params[2];
            String edad=params[3];
            String precio=params[4];
            String pasear=params[5];
            String banar=params[6];
            String alimentar=params[7];
            String cliente=params[8];
            String ubicacion=params[9];
            final String url = "https://gqmsoluciones.com/app/creasolicitud.php?nombre="+nombre+"&raza="+raza+"&peso="+peso+"&edad="+edad+"&precio="+
                    precio+"&pasear="+pasear+"&banar="+banar+"&alimentar="+alimentar+"&cliente="+cliente+"&ubicacion="+ubicacion;
            HttpURLConnection httpURLConnection = null;
            String data = "";
            try {
                httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
                InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                data = parseResult(inputStream);
            } catch (Exception e) {
                //TODO
            } finally {
                if (httpURLConnection != null) {
                    try {
                        httpURLConnection.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return data;
        }

        private String parseResult(InputStream inputStream) {
            BufferedReader bufferedReader = null;
            StringBuffer stringBuffer = new StringBuffer("");
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return stringBuffer.toString();
        }

        protected void onPostExecute(String datos) {
            String d = datos;
            if (d.equals("1")) {
                etNombreM.setText("");
                etRaza.setText("");
                etPeso.setText("");
                etEdad.setText("");
                rdpaseo.setText("");
                rdbanar.setText("");
                rdalimentar.setText("");
                Ubicacion.setText("");
                Intent intent = new Intent(getApplicationContext(), panel_cliente.class);
                startActivity(intent);
                Toast.makeText(getApplication(),"Solicitud Registrada",Toast.LENGTH_LONG).show();

            }
        }
    }
}
