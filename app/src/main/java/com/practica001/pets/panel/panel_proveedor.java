package com.practica001.pets.panel;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.practica001.pets.R;
import com.practica001.pets.proveedor.EstadoAceptadasProveedor;
import com.practica001.pets.proveedor.EstadoFinalizadasProveedor;
import com.practica001.pets.proveedor.solicitudes_pendientes;

public class panel_proveedor extends AppCompatActivity {

    private Button btversolicitud;
    private Button btaceptada;
    private Button btfinalizada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panel_proveedor);


        btversolicitud=findViewById(R.id.btversolicitud);
        btaceptada=findViewById(R.id.btaceptada);
        btfinalizada=findViewById(R.id.btfinalizada);

        btversolicitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), solicitudes_pendientes.class);
                startActivity(intent);
            }
        });
        btaceptada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EstadoAceptadasProveedor.class);
                startActivity(intent);
            }
        });
        btfinalizada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EstadoFinalizadasProveedor.class);
                startActivity(intent);
            }
        });

    }
}
