package com.practica001.pets.panel;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.practica001.pets.R;
import com.practica001.pets.cliente.EstadoAceptadasCliente;
import com.practica001.pets.cliente.EstadoFinalisadasCliente;
import com.practica001.pets.cliente.EstadoPendienteCliente;
import com.practica001.pets.solicitud_clientes;

public class panel_cliente extends AppCompatActivity {

    private Button btSolicitud;
    private Button btpendiente;
    private Button btaceptada;
    private Button btfinalizada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panel_cliente);

        btSolicitud = (Button)findViewById(R.id.btsolicitud);
        btpendiente=findViewById(R.id.btpendiente);
        btaceptada=findViewById(R.id.btaceptada);
        btfinalizada=findViewById(R.id.btfinalizada);

        btSolicitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), solicitud_clientes.class);
                startActivity(intent);
            }
        });
        btpendiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EstadoPendienteCliente.class);
                startActivity(intent);
            }
        });
        btaceptada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EstadoAceptadasCliente.class);
                startActivity(intent);
            }
        });
        btfinalizada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EstadoFinalisadasCliente.class);
                startActivity(intent);
            }
        });
    }
}

