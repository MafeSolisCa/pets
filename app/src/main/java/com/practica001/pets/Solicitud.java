package com.practica001.pets;
public class Solicitud {
    private int Id;
    private String Nombre;
    private String Raza;
    private float Peso;
    private int Edad;
    private String rdPasear;
    private String rdBañar;
    private String rdAlimentar;
    private double precio;
    private String Localizacion;
    private String Cliente;
    private String Proveedor;
    private String Estado;
    public Solicitud(int id, String nombre, String raza, float peso, int edad, String rdPasear, String rdBañar, String rdAlimentar, double precio, String localizacion, String cliente, String proveedor, String estado) {
        Id = id;
        Nombre = nombre;
        Raza = raza;
        Peso = peso;
        Edad = edad;
        this.rdPasear = rdPasear;
        this.rdBañar = rdBañar;
        this.rdAlimentar = rdAlimentar;
        this.precio = precio;
        Localizacion = localizacion;
        Cliente = cliente;
        Proveedor = proveedor;
        Estado = estado;
    }

    public Solicitud() {
    }
    public int getId() {
        return Id;
    }
    public void setId(int id) {
        Id = id;
    }
    public String getNombre() {
        return Nombre;
    }
    public void setNombre(String nombre) {
        Nombre = nombre;
    }
    public String getRaza() {
        return Raza;
    }
    public void setRaza(String raza) {
        Raza = raza;
    }
    public float getPeso() {
        return Peso;
    }
    public void setPeso(float peso) {
        Peso = peso;
    }
    public int getEdad() {
        return Edad;
    }
    public void setEdad(int edad) {
        Edad = edad;
    }
    public String getRdPasear() {
        return rdPasear;
    }
    public void setRdPasear(String rdPasear) {
        this.rdPasear = rdPasear;
    }
    public String getRdBañar() {
        return rdBañar;
    }
    public void setRdBañar(String rdBañar) {
        this.rdBañar = rdBañar;
    }
    public String getRdAlimentar() {
        return rdAlimentar;
    }
    public void setRdAlimentar(String rdAlimentar) {
        this.rdAlimentar = rdAlimentar;
    }
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public String getLocalizacion() {
        return Localizacion;
    }
    public void setLocalizacion(String localizacion) {
        Localizacion = localizacion;
    }
    public String getCliente() {
        return Cliente;
    }
    public void setCliente(String cliente) {
        Cliente = cliente;
    }
    public String getProveedor() {
        return Proveedor;
    }
    public void setProveedor(String proveedor) {
        Proveedor = proveedor;
    }
    public String getEstado() {
        return Estado;
    }
    public void setEstado(String estado) {
        Estado = estado;
    }
}
