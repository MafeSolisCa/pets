package com.practica001.pets;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.practica001.pets.panel.panel_cliente;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Login_Cliente extends AppCompatActivity {

    private EditText etCorreoCliente;
    private EditText etContraseñaCliente;
    private Button btIniciarSecionC;
    private TextView tvregiC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__cliente);

        etCorreoCliente = (EditText) findViewById(R.id.etCorreoCliente);
        etContraseñaCliente = (EditText) findViewById(R.id.etpsw);
        btIniciarSecionC = findViewById(R.id.btIniciarSecionC);
        tvregiC = findViewById(R.id.tvregiC);


        btIniciarSecionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo =(etCorreoCliente.getText().toString());
                String psw =(etContraseñaCliente.getText().toString());

                new ConsumeWS().execute(correo, psw);//Llama al proceso del servicio web
            }
        });


        tvregiC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),registro_clientes.class);
                startActivity(intent);

            }
        });


    }
   private class ConsumeWS extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String correo=params[0];
            String psw=params[1];
            //?correo="+correo+"&psw="+psw
            final String url = "https://gqmsoluciones.com/app/login.php?correo="+correo+"&psw="+psw;
            HttpURLConnection httpURLConnection = null;
            String data = "8";
            try {
                httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
                InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                data = parseResult(inputStream);
            } catch (Exception e) {
                //TODO
            } finally {
                if (httpURLConnection != null) {
                    try {
                        httpURLConnection.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return data;
        }

        private String parseResult(InputStream inputStream) {
            BufferedReader bufferedReader = null;
            StringBuffer stringBuffer = new StringBuffer("");
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return stringBuffer.toString();
        }

        protected void onPostExecute(String datos) {
            String d = datos;

            //etCorreoCliente.setText(d);
           if (d.equals("1")) {
                etCorreoCliente.setText("");
                etContraseñaCliente.setText("");
                Intent intent = new Intent(getApplicationContext(), panel_cliente.class);
                startActivity(intent);

            }else if(d.equals("2") ){
                Toast.makeText(getApplication(),"Contraseña invalida",Toast.LENGTH_LONG).show();

            }else if(d.equals("3")){
                Toast.makeText(getApplication(),"usuario no existe",Toast.LENGTH_LONG).show();
            }


        }
        }
    }

