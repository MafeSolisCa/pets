package com.practica001.pets.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.practica001.pets.R;
import com.practica001.pets.Solicitud;

import java.util.List;

public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.SolicitudViewHolder> {

    private Context mCtx;
    private List<Solicitud> solicitudList;

    public ClienteAdapter(Context mCtx, List<Solicitud> solicitudList) {
        this.mCtx=mCtx;
        this.solicitudList=solicitudList;
    }
    public SolicitudViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
                View view = inflater.inflate(R.layout.card_solicitud,null);
        return new SolicitudViewHolder(view);
    }
    @Override
    public void onBindViewHolder(SolicitudViewHolder holder, int position) {
        Solicitud solicitud = solicitudList.get(position);

        holder.Nombre.setText(solicitud.getNombre());
        holder.Raza.setText(solicitud.getRaza());
        holder.Peso.setText(String.valueOf(solicitud.getPeso()));
        holder.Edad.setText(String.valueOf(solicitud.getEdad()));
        holder.rdPasear.setText(solicitud.getRdPasear());
        holder.rdBañar.setText(solicitud.getRdBañar());
        holder.rdAlimentar.setText(solicitud.getRdAlimentar());
    }
    @Override
    public int getItemCount() {
        return solicitudList.size();
    }


    public class SolicitudViewHolder extends RecyclerView.ViewHolder {
        //Atributos para renderizar una card
        TextView Nombre;
        TextView Raza;
        TextView Peso;
        TextView Edad;
        RadioButton rdPasear;
        RadioButton rdBañar;
        RadioButton rdAlimentar;

        public SolicitudViewHolder(@NonNull View itemView) {
            super(itemView);
            this.Nombre = itemView.findViewById(R.id.tvNombreM);
            this.Raza = itemView.findViewById(R.id.tvRaza);
            this.Peso = itemView.findViewById(R.id.tvPeso);
            this.Edad = itemView.findViewById(R.id.tvEdad);
            this.rdPasear = itemView.findViewById(R.id.rdPasear);
            this.rdBañar = itemView.findViewById(R.id.rdBañar);
            this.rdAlimentar = itemView.findViewById(R.id.rdAlimentar);


        }
    }
}