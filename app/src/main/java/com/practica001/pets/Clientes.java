package com.practica001.pets;

public class Clientes {
    String correo,psw,nombre,apellidos,telefono;

    public Clientes() {
    }

    public Clientes(String correo, String psw, String nombre, String apellidos, String telefono) {
        this.correo = correo;
        this.psw = psw;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public String getPsw() {
        return psw;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
