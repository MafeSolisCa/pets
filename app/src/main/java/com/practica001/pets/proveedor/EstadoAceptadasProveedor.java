package com.practica001.pets.proveedor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.practica001.pets.R;
import com.practica001.pets.Solicitud;
import com.practica001.pets.adapter.AceptadasProveedorAdapter;
import com.practica001.pets.adapter.ClienteAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EstadoAceptadasProveedor extends AppCompatActivity {

    private static final String URL_soli = "https://gqmsoluciones.com/app/solicitudes.php";

    List<Solicitud> playerList;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado_aceptadas_proveedor);
        recyclerView = findViewById(R.id.reciclador4);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        playerList = new ArrayList<>();
        loadCliente();

    }
    private void loadCliente() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_soli,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray array = new JSONArray(response);

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject cliente = array.getJSONObject(i);

                                playerList.add(new Solicitud(
                                        cliente.getInt(("id")),
                                        cliente.getString(("nombre")),
                                        cliente.getString(("raza")),
                                        cliente.getInt(("peso")),
                                        cliente.getInt(("edad")),
                                        cliente.getString(("pasear")),
                                        cliente.getString(("banar")),
                                        cliente.getString(("alimentar")),
                                        cliente.getDouble(("precio")),
                                        cliente.getString(("ubicacion")),
                                        cliente.getString((("cliente"))),
                                        cliente.getString(("proveedor")),
                                        cliente.getString(("estado"))
                                ));

                            }
                            AceptadasProveedorAdapter adapter = new AceptadasProveedorAdapter(EstadoAceptadasProveedor.this, playerList);
                            recyclerView.setAdapter(adapter);

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
