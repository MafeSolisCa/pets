package com.practica001.pets;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class registro_clientes extends AppCompatActivity {
    private EditText etcorreo;
    private EditText etpsw;
    private EditText etnombre;
    private EditText etapellidos;
    private EditText ettelefono;
    private Button btregistrarc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_clientes);


        etcorreo =  (EditText) findViewById(R.id.etcorreo);
        etpsw = (EditText) findViewById(R.id.etpsw);
        etnombre = (EditText) findViewById(R.id.etnombre);
        etapellidos = (EditText) findViewById(R.id.etapellidos);
        ettelefono = (EditText) findViewById(R.id.ettelefono);
        btregistrarc= (Button) findViewById(R.id.btregistrarc);

        btregistrarc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //registrocliente();
                String correo =(etcorreo.getText().toString());
                String psw =(etpsw.getText().toString());
                String nombre =(etnombre.getText().toString());
                String apellido =(etapellidos.getText().toString());
                String telefono =(ettelefono.getText().toString());
                new ConsumeWS().execute(correo, psw, nombre, apellido,telefono);//Llama al proceso del servicio web
            }
        });

    }



    private class ConsumeWS extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String correo=params[0];
            String psw=params[1];
            String nombre=params[2];
            String apellido=params[3];
            String telefono=params[4];
            final String url = "https://gqmsoluciones.com/app/crearcliente.php?correo="+correo+"&psw="+psw+"&nombre="+nombre+"&apellido="+apellido+"&telefono="+telefono;
            HttpURLConnection httpURLConnection = null;
            String data = "";
            try {
                httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
                InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                data = parseResult(inputStream);
            } catch (Exception e) {
                //TODO
            } finally {
                if (httpURLConnection != null) {
                    try {
                        httpURLConnection.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return data;
        }

        private String parseResult(InputStream inputStream) {
            BufferedReader bufferedReader = null;
            StringBuffer stringBuffer = new StringBuffer("");
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return stringBuffer.toString();
        }

        protected void onPostExecute(String datos) {
                String d = datos;
                if (d.equals("1")) {
                    etcorreo.setText("");
                    etpsw.setText("");
                    etnombre.setText("");
                    etapellidos.setText("");
                    ettelefono.setText("");
                   Intent intent = new Intent(getApplicationContext(),Login_Cliente.class);
                   startActivity(intent);
                    Toast.makeText(getApplication(),"Cliente Registrado",Toast.LENGTH_LONG).show();

                }
            }
        }

    }

